#
# NOTE: THIS DOCKERFILE IS GENERATED VIA "make update"! PLEASE DO NOT EDIT IT DIRECTLY.
#

FROM postgres:16-bullseye

LABEL maintainer="PostGIS Project - https://postgis.net" \
  org.opencontainers.image.description="PostGIS 3.4.2+dfsg-1.pgdg110+1 spatial database extension with PostgreSQL 16 bullseye" \
  org.opencontainers.image.source="https://github.com/postgis/docker-postgis"

ENV POSTGIS_MAJOR 3
ENV POSTGIS_VERSION 3.4.2+dfsg-1.pgdg110+1

RUN apt-get update \
  && apt-cache showpkg postgresql-$PG_MAJOR-postgis-$POSTGIS_MAJOR \
  && apt-get install -y --no-install-recommends \
  && apt-get install postgresql-contrib \
  && apt-get install -y postgresql-plpython3-16  \
  && apt-get install -y python3-pip \
  ca-certificates \
  \
  postgresql-$PG_MAJOR-postgis-$POSTGIS_MAJOR=$POSTGIS_VERSION \
  postgresql-$PG_MAJOR-postgis-$POSTGIS_MAJOR-scripts 

RUN mkdir -p /docker-entrypoint-initdb.d \
  && rm -rf /var/lib/apt/lists/* \
  && pip install requests \ 
  && pip install jsonschema

COPY ./initdb.sh /docker-entrypoint-initdb.d/10_postgis.sh
COPY ./pg-http.sql /tmp
COPY ./pg-util.sql /tmp
COPY ./pg-router.sql /tmp