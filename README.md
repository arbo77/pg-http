# pg-http

The PostgreSQL image provides tags for running Postgres with PostGIS, plpython3u and pgcrypto extensions installed. This image is based on the official postgis image and provides debian and alpine variants for PostGIS 3.4.x, which is compatible with PostgreSQL versions 12, 13, 14, 15, and 16. Additionally, an image version is provided which is built from the latest two versions of Postgres (15, 16) with versions of PostGIS and its dependencies built from their respective master branches.

This image ensures that the default database created by the parent `postgres` image will have the following extensions installed:

| installed extensions | initialized |
| -------------------- | ----------- |
| `postgis`            | yes         |
| `plpython3u`         | yes         |
| `pgcrypto`           | yes         |

### HTTP Schema

This image also provides basic HTPP request functions written in plpython3u.
Currently these functions only accept JSON response.

| http method | function name | example                                                                    |
| ----------- | ------------- | -------------------------------------------------------------------------- |
| `GET`       | http.get      | `select http.post('https://httpbin.org/post','{"data":{"a": 1,"b": 2}}');` |
| `POST`      | http.post     | `select http.get('https://httpbin.org/get');`                              |
