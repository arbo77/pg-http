#!/bin/bash

set -e

# Perform all actions as $POSTGRES_USER
export PGUSER="$POSTGRES_USER"

# # Create the 'template_postgis' template db
# "${psql[@]}" <<- 'EOSQL'
# CREATE DATABASE template_postgis IS_TEMPLATE true;
# EOSQL

# Load PostGIS into  $POSTGRES_DB
echo "Loading PostGIS, Python and Crypto extensions into $POSTGRES_DB"
psql --dbname $POSTGRES_DB -c "CREATE EXTENSION IF NOT EXISTS postgis;"
psql --dbname $POSTGRES_DB -c "CREATE EXTENSION IF NOT EXISTS plpython3u;"
psql --dbname $POSTGRES_DB -c "CREATE EXTENSION IF NOT EXISTS pgcrypto;"
psql --dbname $POSTGRES_DB -a -f /tmp/pg-http.sql
psql --dbname $POSTGRES_DB -a -f /tmp/pg-util.sql
psql --dbname $POSTGRES_DB -a -f /tmp/pg-router.sql
