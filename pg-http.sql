create schema http;

CREATE OR REPLACE FUNCTION http.fallback(code varchar, message text)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
begin
	return json_build_object('status',false, 'error', json_build_object('code',code ,'message',message));
end;
$function$
;

CREATE OR REPLACE FUNCTION http._get(url varchar, params jsonb DEFAULT NULL::jsonb)
 RETURNS text
 LANGUAGE plpython3u
AS $function$
	import json
	import requests

	if params is None:
		resp = requests.get(url)
	else:
		resp = requests.get(url, tuple(json.loads(params)))

	return resp.text
$function$
;

CREATE OR REPLACE FUNCTION http._post(url varchar, params jsonb DEFAULT NULL::jsonb)
 RETURNS text
 LANGUAGE plpython3u
AS $function$
	import json
	import requests

	resp = requests.post(
		url,
		json = json.loads(params),
		headers = {"charset": "utf-8", "Content-Type": "application/json"}
	)
	return resp.text
$function$
;

CREATE OR REPLACE FUNCTION http.get(url varchar, params jsonb DEFAULT NULL::jsonb)
 RETURNS jsonb
 LANGUAGE plpgsql
AS $function$
begin
	return http._get(url,params)::jsonb;
exception
when others then
	return http.fallback(sqlstate,sqlerrm);
end;
$function$
;

CREATE OR REPLACE FUNCTION http.post(url varchar, params jsonb)
 RETURNS jsonb
 LANGUAGE plpgsql
AS $function$
begin
	return http._post(url,params)::jsonb;
exception
when others then
	return http.fallback(sqlstate,sqlerrm);
end;
$function$
;