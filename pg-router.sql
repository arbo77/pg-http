create schema router;


CREATE OR REPLACE FUNCTION router.dispatch(request jsonb) RETURNS jsonb LANGUAGE plpgsql AS $function$
	begin
		return jsonb_build_object('status', true, 'data', jsonb_build_object('request', request), 'timestamp', util.now());
	end;
$function$ ;


CREATE OR REPLACE FUNCTION router.dispatch(action character varying, request jsonb) RETURNS jsonb LANGUAGE plpython3u AS $function$
	import json
	r = json.loads(request)
	p = plpy.prepare("select " + action + "($1) as resp", ['jsonb'])
	r = plpy.execute(p,[json.dumps(r["body"])])
	return r[0]['resp']
$function$ ;


CREATE OR REPLACE FUNCTION router.echo(request jsonb) RETURNS jsonb LANGUAGE plpgsql AS $function$
	begin
		return jsonb_build_object('status', true, 'data', jsonb_build_object('request', request), 'timestamp', util.now());
	end;
$function$ ;


CREATE OR REPLACE FUNCTION router.reply(jsonb) RETURNS jsonb LANGUAGE plpgsql AS $function$
begin
	return jsonb_build_object(
		'status', true,
		'data', $1,
		'timestamp', util.now()
	);
end;
$function$ ;

