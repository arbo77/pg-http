create schema util;


CREATE OR REPLACE FUNCTION util.timestamp(ts TIMESTAMP) RETURNS BIGINT LANGUAGE plpgsql AS $function$
begin
	return trunc(extract(epoch from ts)*1000);
end;
$function$ ;


CREATE OR REPLACE FUNCTION util.timestamp(ts TIMESTAMPTZ) RETURNS BIGINT LANGUAGE plpgsql AS $function$
begin
	return trunc(extract(epoch from ts)*1000);
end;
$function$ ;